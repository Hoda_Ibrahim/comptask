@extends('layout.master')

@section('content')
    <div id="freshItems" class="col-md-12">
    <table class="table table-bordered table-striped" id="products_table">
        <thead>
        <tr>
            <th width="5%">Image</th>
            <th width="30%">Product Name</th>
            <th width="30%">Product Description</th>
            <th width="15%">Toggile Favourite</th>
        </tr>
        </thead>
        <tbody>
        @foreach( $products as $product)
            <tr>
                <td>
                    <img src="{{ $product->product_img }}" width="100px" height="100px">
                </td>
                <td>
                    {{ $product->product_name }}
                </td>
                <td>
                    {{ $product->product_desc }}
                </td>
                <td>
                    <button data-productid="{{ $product->id }}" id="controlFavourite{{ $product->id }}" class="favourite btn {{ $product->favourite == true ? "btn-success" : "btn-danger" }}">
                        {{ $product->favourite == true ? "Favourite" : "NOT Favourite" }}
                    </button>
                </td>

            </tr>
        @endforeach
        </tbody>
    </table>
    </div>
@endsection
@section('js')
    <script>
        $(document).ready(function() {
            $(document).on("click", ".favourite", function (event) {
                var target = event.target || event.srcElement;

               if (target.innerHTML.trim() != "Favourite" ) {
                   var id = event.currentTarget.dataset.productid;
                   $.ajax({
                       headers: {
                           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                       },
                       url: "products/" + id,
                       type: "PUT",
                       data: {"_method": 'PUT', id: id},
                       success: function (data) {
                           $('#freshItems').load(window.location.href + " #freshItems");
                       }
                   })
               }
            });
        });
            $('.favorite, .insert').removeClass('active');
            $('.all').addClass('active');
    </script>
@endsection