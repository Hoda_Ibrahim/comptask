@extends('layout.master')
@section('css')
    <style>
        #nameError, #descError, #imgError {
            color: red;
            padding-left: 12px;
        }
    </style>
@endsection
@section('content')

    <div class="col-md-12">
    @include('layout.errors')
        <!-- Horizontal Form -->
        <div class="box box-info" style="border-top: 3px solid #00c0ef;background-color: white;padding: 10px;border-radius: 4px">

            <div class="box-header with-border">
                <h3 class="box-title">Insert Product</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form id="file" class="form-horizontal" method="POST" enctype="multipart/form-data" action="{{ url('/products') }}">
                @csrf
                <div class="box-body">
                    <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">Product Name</label>

                        <div class="col-sm-10">
                            <input required name="product_name" type="text" class="form-control" id="name" placeholder="Product Name">
                            <span id="nameError"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="desc" class="col-sm-2 control-label">Description</label>

                        <div class="col-sm-10">
                            <textarea  required name="product_desc" class="form-control" id="desc" placeholder="Description"></textarea>
                            <span id="descError"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="img" class="col-sm-2 control-label">Image</label>

                        <div class="col-sm-10">
                            <input required name="product_img" type="file" id="img" class="form-control" accept=".png, .jpg">
                            <span id="imgError"></span>
                        </div>
                    </div>

                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-default">Cancel</button>
                    <button type="submit" class="btn btn-info pull-right" id="submit">Sign in</button>
                </div>
                <!-- /.box-footer -->
            </form>
        </div>
        <!-- /.box -->
    </div>
@endsection
@section('js')
    <script>
        $(document).ready(function() {
            $('.all, .favourite').removeClass('active');
            $('.insert').addClass('active');
            $(document).on('click', '#submit', function (e) {
                $('#nameError').empty();
                $('#descError').empty();
                $('#imgError').empty();
                var re = new RegExp("^([a-z0-9 ]{5,})$");
                if (!re.test($('#name').val())){
                    $('#nameError').text("Project Name should be Letters Or Numbers and more than 5 chars.");
                    e.preventDefault();
                }

                var re2 = new RegExp("^([a-z0-9 ]{20,})$");
                if (!re2.test($.trim($("#desc").val()))){
                    $('#descError').text("Project Description should be Letters Or Numbers and more than 20 chars.");
                    e.preventDefault();
                }

                if($('#img').val() == "") {
                    $("#imgError").text("Image is required");
                    e.preventDefault();
                } else if ($('#img').val() != "" &&  $('#img')[0].files[0].size>2097152){

                    $("#imgError").text("Image size is greater than 2MB");
                    e.preventDefault();
                    console.log(file_size)
                }
            })
        });
    </script>
@endsection