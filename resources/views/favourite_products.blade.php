@extends('layout.master')

@section('content')
    <div id="freshItems" class="col-md-12">
        <table class="table table-bordered table-striped" id="products_table">
            <thead>
            <tr>
                <th width="5%">Image</th>
                <th width="30%">Product Name</th>
                <th width="30%">Product Description</th>
                <th width="15%">Toggile Favourite</th>
            </tr>
            </thead>
            <tbody>
            @foreach( $products as $product)
                <tr>
                    <td>
                        <img src="{{ $product->product_img }}" width="100px" height="100px">
                    </td>
                    <td>
                        {{ $product->product_name }}
                    </td>
                    <td>
                        {{ $product->product_desc }}
                    </td>
                    <td>
                        <button data-productid="{{ $product->favouriteProduct->id }}" id="delete_fom_favourite_products" class="btn btn-danger">
                            Remove From Favourites
                        </button>
                    </td>

                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
@section('js')
    <script>
        $(document).ready(function() {
            $(document).on("click", "#delete_fom_favourite_products", function () {
                var id = $('#delete_fom_favourite_products').data('productid');
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "delete_fom_favourite_products",
                    type: "DELETE",
                    data: {"_method": 'DELETE', id: id},
                    success: function (data) {
                        $('#freshItems').load(window.location.href + " #freshItems");
                    }
                })
            });
        });

        $('.insert, .all').removeClass('active');
        $('.favorite').addClass('active');
    </script>
@endsection