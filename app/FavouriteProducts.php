<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FavouriteProducts extends Model
{
    protected $table="favourite_products";
    protected $fillable = ['product_id'];
}
