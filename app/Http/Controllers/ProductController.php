<?php

namespace App\Http\Controllers;

use App\FavouriteProducts;
use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        return view('products', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('insert_product');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $valedated_date = request()->validate([
                'product_name' => 'required|min:5|max:50',
                'product_desc' => 'required|min:20|max:250',
                'product_img' => 'required|file|image|max:5000'
            ]);
            if (request()->has('product_img')) {
                $image = request()->file('product_img');
                $name = $image->getClientOriginalName();
                $dir = 'products-attachments/' . $name;
                $destinationPath = public_path("/products-attachments/");
                $image->move($destinationPath, $name);
                $valedated_date['product_img'] = $dir;
            }
            Product::create($valedated_date);
            return redirect('products');
            } catch (\Exception $ex) {
                return [
                    'status' => 'danger',
                    'message' => 'Insertion Failed',
                    'ex' => $ex->getMessage()
                ];
            }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $product = Product::find($request->id);
        dump($product->id);
        try {
            FavouriteProducts::create([
                'product_id' => $product->id,
            ]);
            $product->update([
                'favourite' => true
            ]);
        }catch (\Exception $ex) {
            return [
                'status' => 'danger',
                'message' => 'Insertion Failed',
                'ex' => $ex->getMessage()
            ];
        }
        //return "done";
    }
    
    public function removeFromFavourite(Request $request)
    {
        try{
            $fav_product = FavouriteProducts::find($request->id);
            
            $product = Product::find($fav_product->product_id);
            $product->update([
                'favourite' => false
            ]);
            
            $fav_product->delete($fav_product->product_id);
            
        } catch (\Exception $ex) {
            return [
                'status' => 'danger',
                'message' => 'Deletion Failed',
                'ex' => $ex->getMessage()
            ];
        }
        
    }
    
    public function favouriteProducts(){
        $favourites_ids = FavouriteProducts::all()->pluck('product_id')->toArray();
        $products = Product::whereIn('id', $favourites_ids)->get();
        return view('favourite_products', ['products' => $products, 'class' => 'fav']);
    }
}
