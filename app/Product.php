<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table="products";
    protected $fillable = ['product_name', 'product_desc', 'product_img', 'favourite'];
    public function favouriteProduct(){
        return $this->hasOne(FavouriteProducts::class);
    }
}
