<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('favourite_products', 'ProductController@favouriteProducts');
Route::delete('delete_fom_favourite_products', 'ProductController@removeFromFavourite');
Route::resources([
    'products' => 'ProductController',
]);